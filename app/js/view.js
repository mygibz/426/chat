async function displayChatContent(result) {
    // console.log(result);
    element = document.getElementById('chatContent');

    output = "";

    for (var message of result)

        // Generate output
        output += `
            <div class="card horizontal">
                <div class="card-stacked">
                    <div class="card-content" style="display: flex;">

                        <img src="${await getPfpLink(message.userId)}" alt="${await getUserName(message.userId)}" onerror="this.src='https://robohash.org/${message.username}.png'" class="pfp tooltipped" data-position="left" data-tooltip="${await getUserName(message.userId)}">

                        <div>
                            <p>${message.message}</p>
                        </div>

                    </div>
                </div>
            </div>
        `;

    element.innerHTML = output;

    updateTooltips();
}

function fillTeamContent() {
    get(`${mainAPIpath}/teams/user/1`, {}).then(response => {
        var team = response;
        let element = document.getElementById('teamsContent');
        let selectedChannel = getUrlParameter('channel');

        output = "";
        for (var team of response) {
            
            output += `<ul class="collection with-header">
                    <li class="collection-header"><h4>${team.teamName}</h4></li>`;
                
            team.channels.forEach(channel => {
                let active = (selectedChannel == channel.channelId) ? "active" : "";
                output += `<li class="collection-item ${active}" onclick="switchChannel(${channel.channelId})"><div>${channel.channelName}</li>`;
            });

            output += `</ul>`;

        }

        element.innerHTML = output;
    })
}

setTimeout(displayChatContent, 200);