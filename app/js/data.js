const mainAPIpath = "http://localhost:8080";

function fillChatContent() {
    get(`${mainAPIpath}/message/${getUrlParameter('channel')}`, {
    }).then( response => {
        displayChatContent(response);
    })
}

function registerUser () {
    fetch(`${mainAPIpath}/user`, { 
        method: "POST", 
        
        body: JSON.stringify({ 
            username: getElement('registerUsername').value, 
            password: md5(getElement('registerUsername').value + getElement('registerPassword').value), 
            pfpLink: getElement('registerPfpUrl').value
        }), 
        
        headers: { 
            "Content-type": "application/json; charset=UTF-8"
        } 
    }).then(response => response.json()).then(json => console.log(json)); 
}

function sendMessage() {
    post(`${mainAPIpath}/message/${getUrlParameter('channel')}`, {
        userId: localStorage.getItem("loginUser"),
        message: getContent('messageBox')
    }).then( response => {
        if (response.status == 200) {
            fillChatContent();
        } else M.toast({html: response.message});
    })
    getElement('messageBox').value = "";
}

async function getPfpLink(userId) {
    if (localStorage.getItem("pfp" + userId) != null) {
        return localStorage.getItem("pfp" + userId);
    } else {
        let result = await get(`${mainAPIpath}/user/${userId}`, {});
        localStorage.setItem("pfp" + userId, result.imageURL);
        return result.imageURL;
    }
}
async function getUserName(userId) {
    if (localStorage.getItem("name" + userId) != null) {
        return localStorage.getItem("name" + userId);
    } else {
        let result = await get(`${mainAPIpath}/user/${userId}`, {});
        localStorage.setItem("name" + userId, result.username);
        return result.imageURL;
    }
}