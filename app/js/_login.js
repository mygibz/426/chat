getElement('loginSubmitButton').onclick = () => {
    // TODO: Add Checks
    post(`${mainAPIpath}/login/check/`, {
        username: getContent('loginUsername'),
        password: md5(getContent('loginPassword')),
    }).then(response => {
        
        if (response.status == 200) {
            M.toast({html: "You've been logged in successfully!"});
            M.Modal.getInstance(getElement('loginModal')).close();
            performLogin(response.userId);
        } else M.toast({html: response.message});

    })
}