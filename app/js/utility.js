function switchChannel(channelId) {
    history.pushState({}, '', `?channel=${channelId}`);
    fillTeamContent();
    fillChatContent();
}

/* Assistive functions */
function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam)
            return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
    }
    return false;
}

const getElement = (id)  => { return document.getElementById(id) }
const getContent = (id)  => { return document.getElementById(id).value }

const addJS2Dom = (source) => {
    var script = document.createElement('script'); 
    script.src = source; 
    document.head.appendChild(script) ;
}

/* Login, Logout */

const performLogin = (userId) => {
    // TODO: Actually implement
    localStorage.setItem("loginUser", userId);

    // Refresh messages and Teams
    fillChatContent();
    fillTeamContent();
}

const performLogout = () => {
    localStorage.clear();
    location.href = ".";
}

const preformRefresh = () => {
    let tmpUserId = localStorage.getItem("loginUser");
    localStorage.clear();
    localStorage.setItem("loginUser", tmpUserId);

    fillChatContent();
    fillTeamContent();
}


/* Materialize */
function updateTooltips() {
    M.Tooltip.init(document.querySelectorAll('.tooltipped'), {});
}

/* POST and GET requests */
const request = ( url, params = {}, method = 'GET' ) => {
    let options = {
        method,
        headers: {
            "token": "udhsfilgmgujtldhtuvbtbmcxbhtrn8s7mhrvurjvveiusfhw7c48rz578wtghw67gw78ohmctwnhotwrh suyukrcghtikmshmr5h8s8o", 
            'Content-Type': 'application/json;charset=utf-8'
        }
    };

    if (method == 'GET')
        url += '?' + ( new URLSearchParams(params) ).toString();
    else
        options.body = JSON.stringify(params);
    
    return fetch( url, options ).then( response => response.json() );
};

const get = ( url, params ) => request( url, params, 'GET' );
const post = ( url, params ) => request( url, params, 'POST' );