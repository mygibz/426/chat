getElement('registerSubmitButton').onclick = () => {
    // // TODO: Add Checks
    post(`${mainAPIpath}/user`, {
        username: getContent('registerUsername'),
        password: md5(getContent('registerPassword')),
        imageURL: getContent('registerPfpUrl')
    }).then( response => {

        if (response.status == 200) {
            M.toast({html: "The user has been created!"});
            performLogin(response.userId);
            M.Modal.getInstance(getElement('registerModal')).close();
        } else M.toast({html: response.message});

    })

}