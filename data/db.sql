CREATE TABLE `channel` (
  `channelId` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `teamId` INTEGER NOT NULL,
  `channelName` varchar(50) NOT NULL
);

CREATE TABLE `messages` (
  `messageId` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `message` varchar(300) NOT NULL,
  `channelId` INTEGER NOT NULL,
  `userId` INTEGER NOT NULL
);

CREATE TABLE `team` (
  `teamId` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `teamName` varchar(100) NOT NULL
);

CREATE TABLE `user` (
  `userId` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `username` varchar(50) NOT NULL,
  `password` char(32) NOT NULL,
  `imageURL` varchar(100) NOT NULL
);

CREATE TABLE `userInTeams` (
  `userId` INTEGER NOT NULL,
  `teamId` INTEGER NOT NULL
);