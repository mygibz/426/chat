const cors = require('cors');
const express = require('express');
const app = express();
const sqlite3 = require('sqlite3').verbose();

app.use(express.json());
app.use(cors());

let token = "udhsfilgmgujtldhtuvbtbmcxbhtrn8s7mhrvurjvveiusfhw7c48rz578wtghw67gw78ohmctwnhotwrh suyukrcghtikmshmr5h8s8o";

// Connect to db
let db = new sqlite3.Database('../data/db.sqlite', (err) => {
    if (err){
        return console.error("Connection to database unsuccessful: " + err.message);
    }
    console.log("Connected to database");
});

// Listener on port 8080 for api requests
let server = app.listen(8080, () => {
    let host = server.address().address
    let port = server.address().port
    console.log("Enabled app listening at http://%s:%s", host, port);
});

app.get('', (req, res) => {
    res.status(418);
    res.end("Hey! Looks like you mistook the website with the api.");
});

/******************************************************* USER *********************************************************/

// Get userinfo by id
app.get('/user/:id', (req, res) => {
    db.serialize(() => {
        let header = req.headers
        if (header["token"] !== token){
            res.status(401);
            res.end("Unauthorized request: Improper token!");
            console.error("Unauthorized request detected!");
            return;
        }
        db.get(`SELECT username, imageURL FROM user WHERE userId = '${req.params.id}'`, (err, row) => {
            if (err){
                res.status(500);
                res.end("Whoops! Something went wrong on out side.");
                console.error("Error while fetching user: " + err.message);
                return;
            }
            if (row === undefined){
                res.status(404);
                res.end("Unknown user!");
                return;
            }
            res.status(200);
            res.end(JSON.stringify(row));
        });
    });
});

// Create user
app.post('/user', (req, res) => {
    db.serialize(() => {
        let header = req.headers
        if (header["token"] !== token){
            res.status(401);
            res.end("Unauthorized request: Improper token!");
            console.error("Unauthorized request detected!");
            return;
        }
        let body = req.body;
        db.get(`SELECT COUNT(userId) as 'Count' FROM user WHERE username = '${body["username"]}'`, (err, row) => {
            if (err){
                res.status(500);
                res.end(JSON.stringify({status: res.statusCode, message:"Whoops! Something went wrong on our end."}));
                console.error("Error while creating user: " + err.message);
                return;
            }
            if (JSON.stringify(row["Count"]) !== "0"){
                res.status(400);
                res.end(JSON.stringify({status: res.statusCode, message:"User with that name already exists!"}));
                return;
            }
            db.run(`INSERT INTO user (username, password, imageURL) VALUES ('${body["username"]}', '${body["password"]}', '${body["imageURL"]}')`, function(err){
                if (err){
                    res.status(500);
                    res.end(JSON.stringify({status: res.statusCode, message:"Whoops! Something went wrong on our end."}));
                    console.error("Error while creating user: " + err.message);
                    return;
                }
                db.get(`SELECT userId FROM USER WHERE username = '${body["username"]}'`, (err, row) => {
                    if (err){
                        res.status(500);
                        res.end(JSON.stringify({status: res.statusCode, message:"Error while fetching created user. User has been created."}));
                        console.log("Error while fetching new user: " + err.message);
                        return;
                    }
                    res.status(200);
                    res.end(JSON.stringify({status: res.statusCode, userId: row["userId"]}));
                });
            });
        });
    });
});

// Delete user
app.delete('/user/:id', (req, res) => {
    db.serialize(() => {
        let header = req.headers
        if (header["token"] !== token){
            res.status(401);
            res.end("Unauthorized request: Improper token!");
            console.error("Unauthorized request detected!");
            return;
        }
        db.get(`SELECT userId FROM user WHERE userId = ${req.params.id}`, (err, row) => {
            if (err){
                console.log("Error while fetching user to delete. Trying to delete now.");
            }
            if (row === undefined){
                res.status(404);
                res.end("Unknown user.");
                console.error("Error while deleting user: Unknown user.");
                return;
            }
            db.run(`DELETE FROM user WHERE userId = '${req.params.id}'`, (err) => {
                if (err){
                    res.status(500);
                    res.end(JSON.stringify({status: res.statusCode, message:"Whoops! Something went wrong on our end."}));
                    console.error("Error while deleting user: " + err.message);
                    return;
                }
                res.status(200);
                res.end("User has been deleted.");
            });
        });
    });
});

// Check login
app.post('/login/check/', (req, res) => {
    db.serialize(() => {
        let header = req.headers
        if (header["token"] !== token) {
            res.status(401);
            res.end("Unauthorized request: Improper token!");
            console.error("Unauthorized request detected!");
            return;
        }
        let body = req.body;
        db.get(`SELECT userId, username, password FROM user WHERE username = '${body["username"]}'`, (err, row) => {
            if (err){
                res.status(500);
                res.end("Whoops! Something went wrong on our end.");
                console.log("Error while fetching user to check: " + err.message);
                return;
            }
            if (row === undefined){
                res.status(404);
                res.end(JSON.stringify({status: res.statusCode, message: `Could not find user: '${body["username"]}'`}));
                return;
            }
            if (row["password"] === body["password"]){
                res.status(200);
                res.end(JSON.stringify({status: res.statusCode, userId: row["userId"]}));
            }
            else{
                res.status(401);
                res.end(JSON.stringify({status: res.statusCode, message: "Invalid credentials"}));
            }
        });
    });
});

/****************************************************** TEAM **********************************************************/

// Get teams and channels from specific user
app.get('/teams/user/:id', async (req, res) => {
    
    let getTeams = new Promise(function (resolve) {
        db.all(`SELECT * FROM team INNER JOIN userInTeams ON userId = ${req.params.id}`, (err, teams) => { resolve(teams) } )
    });

    // let getChannels = (id) => {
    //     return new Promise(function (resolve) {
    //         db.all(`SELECT channelId, channelName FROM channel WHERE teamId = '${id}'`, (err, channels) => { resolve(channels) } )
    //     });
    // }

    
    getTeams.then((result) => {
        let teams = [];

        result.forEach((team) => {

            // let channels = getChannels(team.teamId).then((channels) => {
            //     return channels
            // })
            
            // team['channels'] = channels;
            // console.log(team.teamId);
            team['channels'] = [{
                "channelId": 1,
                "channelName": "General",
            },{
                "channelId": 2,
                "channelName": "Talk 1",
            },{
                "channelId": 3,
                "channelName": "Talk 2",
            }];
            teams.push(team);

        });

        // console.log(JSON.stringify(teams));
        res.status(200);
        res.end(JSON.stringify(teams));

    })

});



// Send message
app.post('/message/:channelId', (req, res) => {
    db.serialize(() => {
        let header = req.headers
        if (header["token"] !== token){
            res.status(401);
            res.end("Unauthorized request: Improper token!");
            console.error("Unauthorized request detected!");
            return;
        }
        let body = req.body;
        db.run(`INSERT INTO messages (message, channelId, userId) VALUES ('${body["message"]}', '${req.params.channelId}', '${body["userId"]}')`, function(err){
            if (err){
                res.status(500);
                res.end(JSON.stringify({status: res.statusCode, message:"Whoops! Something went wrong on our end."}));
                console.error("Error while creating user: " + err.message);
                return;
            }
            res.status(200);
            res.end(JSON.stringify({status: res.statusCode, message: "created"}));
        });
    });
});

// Get messages from channel
app.get('/message/:channelId', async (req, res) => {
    
    let getMessages = new Promise(function (resolve) {
        db.all(`SELECT message, userId FROM messages WHERE channelId = '${req.params.channelId}' ORDER BY messageId DESC LIMIT 10`, (err, messages) => { resolve(messages) } )
    });

    getMessages.then((result) => {
        res.end(JSON.stringify(result));
    })

});