<p align="center">
  <a href="https://gitlab.com/mygibz/426/chat">
    <img src="app/favicon.png" alt="Project-Logo">
  </a>

  <h3 align="center">Perle Chat</h3>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Mockup](#mockup)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
* [Roadmap](#roadmap)
* [License](#license)
* [Contact](#contact)



<!-- ABOUT THE PROJECT -->
## About The Project

This is a Project from the M 426. On the [website](https://perlechat.ch) (not available yet) you can chat with other people. Just register or log in and you will see all your groups. Each group has multiple text channels. There you can chat with the other users. If you want to invite someone to a grup just copy the link next to the groupname and send it to the person you want to invite. For best personal usage you can create your own groups. There you can edit all the chats and have nice conversations with your friends.

Here are the most important functions:
* You can create an account to get access to the Perle Chat.
* You can chat with other users in groups.
* You can create new groups and invite other users to them.

Of course, there are hunderts of things they could be added to the site. So we will adding more in the near future.

### Mockup
[Here](https://wireframepro.mockflow.com/view/M6c232db55004b70c83239f0a9d3c8c2b1614593220520) you can see the Mockup of the Perle Chat.It is the first visualization of the project. So due to the deadline and technical simplifications, the result doesn't look exactly like the Mockup.


### Built With
This website is built with:
* [HTML](https://html.com/)
* [CSS](https://html.com/css/)
* [Node JS](https://nodejs.org/en/)
* [Javascript](https://www.javascript.com)
* [Materializecss](https://materializecss.com/)

<!-- GETTING STARTED -->
## Getting Started
Perle Chat is very simple to use. Just go to the website (not avaiable yet). There you can log in or create a new account. Once you hav succesfully logged in, you will see all your groups. Now you can start to chat with your friends. Have fun!

### Prerequisites
There are no special prerequisites. The only thing you need is an internet connection and a browser to visit the website.

<!-- ROADMAP -->
## Roadmap

See the [open issues](https://gitlab.com/mygibz/426/chat/-/issues) for a list of proposed features (and known issues).

<!-- SECURITY -->
## Security
This application is made with secure programming languages and currently the application is very secure.


<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.


<!-- CONTACT -->
## Contact

[Manuel Gwerder](https://gitlab.com/AnotherManuel) - Product Owner / Developer

[Fabian Frey](https://gitlab.com/hoizickzack) - Scrum Master / Developer

[Kai Steinke](https://gitlab.com/kaisteinke) - Developer / Tech Support

[Yanik Amman](https://gitlab.com/ConfusedAnt) - Developer

[Samuel Gartmann](https://gitlab.com/sgart) - Developer

[Risigesan Thayakaran](https://gitlab.com/risiworks) - Developer
